import express from "express";
import { createServer } from "http";
import { ApolloServer, gql } from "apollo-server-express";
import { schema } from "./schema";
import { createContext } from "./context";
import {
  ApolloServerPluginDrainHttpServer,
  ApolloServerPluginLandingPageGraphQLPlayground,
} from "apollo-server-core";

const startServer = async () => {
  const app = express();
  const httpServer = createServer(app);

  const apolloServer = new ApolloServer({
    schema: schema,
    context: createContext,
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }),
      ApolloServerPluginLandingPageGraphQLPlayground(),
    ],
  });

  await apolloServer.start();

  apolloServer.applyMiddleware({ app, path: "/graphql" });

  httpServer.listen({ port: 4000 }, () => {
    console.log("Server is running on port 4000");
  });
};

startServer();
