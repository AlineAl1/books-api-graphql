import { extendType, nonNull, stringArg, booleanArg } from "nexus";

export const createBook = extendType({
  type: "Mutation",
  definition(t) {
    t.nonNull.field("createBook", {
      type: "Book",
      args: {
        title: nonNull(stringArg()),
        description: nonNull(stringArg()),
        author: nonNull(stringArg()),
        published: nonNull(booleanArg()),
        user: nonNull(stringArg()),
      },
      resolve(parent, args, ctx) {
        const newBook = ctx.prisma.book.create({
          data: {
            title: args.title,
            description: args.description,
            author: args.author,
            published: args.published,
            user: {
              connect: {
                id: args.user,
              },
            },
          },
        });

        return newBook;
      },
    });
  },
});
