import { objectType } from "nexus";

export const Book = objectType({
  name: "Book",
  definition(t) {
    t.nonNull.int("id");
    t.nonNull.string("title");
    t.nonNull.string("description");
    t.nonNull.boolean("published");
    t.nonNull.string("author");
    t.nonNull.list.nonNull.field("user", {
      type: "User",
      resolve (parent, args, context) {
        return context.prisma.book
          .findUnique({
            where: {
              id: parent.id,
            },
          })
          .user();
      },
    });
  },
});
