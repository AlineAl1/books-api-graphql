import { nonNull, queryField, list } from "nexus";
import { Book } from "../models/Book";
import { User } from "../models/User";

export const books = queryField("books", {
  type: list(nonNull(Book)),
  resolve: async (parent, args, context) => {
    return await context.prisma.book.findMany();
  },
});

export const users = queryField("users", {
  type: list(nonNull(User)),
  resolve: async (parent, args, context) => {
    return await context.prisma.user.findMany();
  },
});
